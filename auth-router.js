const Router = require("express");
const router = new Router();
const controller = require("./auth-controller");
// TODO: (AM) валидация на символы
const { check } = require("express-validator");
const authMiddleware = require("./middleware/auth-middleware");
const roleMiddleware = require("./middleware/role-middleware");

router.post("/registration", (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  controller.registration(req, res);
});
router.post("/login", controller.login);
router.get("/users", authMiddleware, controller.getUsers);
router.get("/check", authMiddleware, controller.checkAuth);

module.exports = router;
