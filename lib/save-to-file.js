const fs = require("fs");
console.log("saveToFile MODULE");
const saveToFile = (state) => {
  console.log("state", state);
  fs.writeFile("/some.txt", JSON.stringify(state), (err) => {
    if (err) {
      console.error(err);
      return;
    }
    //file written successfully
  });
};

module.exports = saveToFile;
