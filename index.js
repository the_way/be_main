const express = require("express");
const authRouter = require("./auth-router");
const todoRouter = require("./todo-router");
const feedRouter = require("./feed-router");
const PORT = process.env.PORT || 8081;

const app = express();

// TODO: (AM) выяснить почему не работал и где именно нужно писать
const cors = require("cors");
app.use(cors());

app.use(express.json());
app.use("/api/auth", authRouter);
app.use("/api/todo", todoRouter);
app.use("/api/feed", feedRouter);

// app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );
//   res.header("Access-Control-Request-Method", "GET, POST");
//   next();
// });

const start = async () => {
  try {
    app.listen(PORT, () => console.log(`server started on port ${PORT}`));
  } catch (e) {
    console.log(e);
  }
};

start();
