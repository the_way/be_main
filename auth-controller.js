const userState = require("./models/user-state");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
const { secret } = require("./config");
const saveToFile = require("./lib/save-to-file");

const findUser = (inputedName) =>
  userState.filter(({ username }) => {
    return username === inputedName;
  });

const generateAccessToken = (id, roles) => {
  const payload = {
    id,
    roles,
  };
  return jwt.sign(payload, secret, { expiresIn: "24h" });
};

class authController {
  async registration(req, res) {
    try {
      const { username, password } = req.body;
      const candidate = await findUser(username);

      if (candidate[0]) {
        return res
          .status(400)
          .json({ message: "Пользователь с таким именем уже существует" });
      }

      const hashPassword = bcrypt.hashSync(password, 7);

      const user = {
        username,
        password: hashPassword,
      };

      userState.push(user);
      // TODO: docker volumes
      saveToFile(userState);

      return res.json({ message: "Пользователь успешно зарегистрирован" });
    } catch (e) {
      res.status(400).json({ message: "Registration error" });
    }
  }

  async login(req, res) {
    try {
      const { username, password } = req.body;
      console.log("username login, password login", username, password);
      const [user] = await findUser(username);
      if (!user) {
        return res
          .status(400)
          .json({ message: `Пользователь ${username} не найден` });
      }
      console.log("user", user);
      const validPassword = bcrypt.compareSync(password, user.password);
      console.log("validPassword", validPassword);
      if (!validPassword) {
        return res.status(400).json({ message: `Введен неверный пароль` });
      }
      const token = generateAccessToken(user._id, user.roles);
      return res.json({ token });
    } catch (e) {
      console.log(e);
      res.status(400).json({ message: "Login error" });
    }
  }

  async getUsers(req, res) {
    try {
      const users = userState;
      res.json(users);
    } catch (e) {
      console.log(e);
    }
  }
  async checkAuth(req, res) {
    try {
      res.json({ isAuth: true });
    } catch (e) {
      res.json({ isAuth: false });
      console.log(e);
    }
  }
}

module.exports = new authController();
