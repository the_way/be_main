const categories = {
  0: "Sport",
  1: "Politics",
  2: "Education",
  3: "IT",
  4: "Culture",
};

const feedState = [...Array(100)].map((v, i) => ({
  id: i,
  title: `title ${i}`,
  shortDescription: `Short description ${i}`,
  fullDescription: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum ${i}`,
  category: categories[i % 5],
}));

module.exports = feedState;
