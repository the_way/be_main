type Feed = {
  [username: string]: {
    created: {
      [username_timeStamp: string]: {
        title: String;
        description: string;
        tags: string[];
        likes: [];
      };
    };
    read: "postId"[];
    subscribe: "username"[];
    subscribed: "username"[];
  };
};

type Test = {
  [username: string]: {
    created: {
      [username_timeStamp: string]: {
        title: String;
        description: string;
        tags: string[];
        likes: [];
      };
    };
    read: "postId"[];
    subscribe: "username"[];
    subscribed: "username"[];
  };
};

type Questions = {
  [username: string]: {
    title: string;
    description: string;
    image: File;
    category: TCategory;
    variants: string[];
    rightAnswers: number[];
  };
};

type TCategory = string[];
