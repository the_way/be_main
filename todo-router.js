const Router = require("express");
const router = new Router();
const controller = require("./todo-controller");
// TODO: (AM) валидация на символы
const authMiddleware = require("./middleware/auth-middleware");

router.post("/create", authMiddleware, controller.create);
router.post("/edit", authMiddleware, controller.edit);
router.get("/getAll", authMiddleware, controller.getAll);
router.get("/getOne", authMiddleware, controller.getOne);
router.delete("/delete", authMiddleware, controller.delete);

module.exports = router;
