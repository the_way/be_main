const userState = require("./models/user-state");
const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");
const feedState = require("./models/feed");

class authController {
  async create(req, res) {
    try {
      const {
        title,
        description,
        priority,
        category,
        author,
        createDate,
        editDate,
        status,
      } = req.body;
      todoState[author].push({
        title,
        description,
        priority,
        category,
        date,
        status,
      });
      return res.json({ message: "Пост успешно создан" });
    } catch (e) {
      console.log(e);
      res.status(400).json({ message: "Post error" });
    }
  }

  async edit(req, res) {
    try {
      const { username, password } = req.body;
      console.log("username login, password login", username, password);
      const [user] = await findUser(username);
      if (!user) {
        return res
          .status(400)
          .json({ message: `Пользователь ${username} не найден` });
      }
      console.log("user", user);
      const validPassword = bcrypt.compareSync(password, user.password);
      console.log("validPassword", validPassword);
      if (!validPassword) {
        return res.status(400).json({ message: `Введен неверный пароль` });
      }
      const token = generateAccessToken(user._id, user.roles);
      return res.json({ token });
    } catch (e) {
      console.log(e);
      res.status(400).json({ message: "Login error" });
    }
  }

  async getAll(req, res) {
    try {
      const feed = feedState;
      res.json(feed);
    } catch (e) {
      console.log(e);
    }
  }
  async getOne(req, res) {
    try {
      res.json({ isAuth: true });
    } catch (e) {
      res.json({ isAuth: false });
      console.log(e);
    }
  }
  async delete(req, res) {
    try {
      res.json({ isAuth: true });
    } catch (e) {
      res.json({ isAuth: false });
      console.log(e);
    }
  }
}

module.exports = new authController();
